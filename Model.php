<?php

class Model
{
    protected $table;
    private $select;
    private $whereClause;
    private $orderClause;
    private $groupClause;
    private static $connection = null;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        $host = "localhost";
        $dbname = "nhanvien_oop";
        $username = "root";
        $password = "123456";
        if (self::$connection == null) {
            self::$connection = new PDO("mysql:host={$host};dbname={$dbname}", $username, $password);
        }
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }
    public static function query()
    {
        return (new static);
    }

    public function getFirst()
    {
        return $this->get(1);
    }

    public function getAll()
    {
        return $this->get();
    }

    /**
     * @param int $row
     * @return array
     */
    public function get($row = null)
    {
        $query = 'SELECT ';
        if ($this->select != null) {
            $query .= $this->select;
        } else {
            $query .= " * ";
        }
        $query .= ' FROM ' . $this->table;
        if ($this->whereClause != null) {
            $query .= $this->whereClause;
        }
        if ($this->orderClause != null) {
            $query .= ' ' . $this->orderClause;
        }
        if ($this->groupClause != null) {
            $query .= ' ' . $this->groupClause;
        }
        if ($row != null) {
            $query .= " limit {$row} ";
        }
        $res = self::$connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        foreach ($res as $re) {
            $object = new static();
            $object->data = $re;
            $result[] = $object;
        }
        return $result;

    }

    /**
     * @param ...$fields
     * @return $this
     */
    public function select(...$fields)
    {
        $i = 0;
        for ($i; $i < count($fields) - 1; $i++) {
            $this->select .= "{$fields[$i]}, ";
        }
        $this->select .= " $fields[$i] ";
        return $this;
    }

    /**
     * @param string $field
     * @param string $operation
     * @param $value
     * @return $this
     */
    public function where($field, $operation = '=', $value = null)
    {
        if (strtolower($operation) == "like") {
            $this->whereClause = " WHERE {$field} {$operation} \"%{$value}%\" ";
        } else {
            $this->whereClause = " WHERE {$field} {$operation} \"{$value}\" ";
        }
        return $this;
    }

    public function andWhere($field, $operation = '=', $value = null)
    {
        if (strtolower($operation) == "like") {
            $this->whereClause = " AND {$field} {$operation} \"%{$value}%\" ";
        } else {
            $this->whereClause .= " AND {$field} {$operation} \"{$value}\" ";
        }
        return $this;
    }


    /**
     * @param string $field
     * @param string $order
     * @return $this
     */
    public function orderBy($field, $order)
    {
        $this->orderClause = " ORDER BY {$field} {$order}";
        return $this;
    }

    /**
     * @param string $field
     * @return $this
     */
    public function groupBy($field)
    {
        $this->groupClause = " GROUP BY {$field}";
        return $this;
    }
}

?>