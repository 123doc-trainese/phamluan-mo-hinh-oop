<?php
    require_once "Model.php";
    require_once "Staff.php";

    class IT extends Model implements Staff
    {
        protected $table = "it";
        private $attributes = [
            'id',
            'name',
            'luongcung',
            'thuong'
        ];
        public function getSalary()
        {
            return $this->luongcung + $this->thuong;
        }
    }
?>