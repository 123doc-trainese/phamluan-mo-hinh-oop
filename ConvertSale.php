<?php
require_once "Model.php";
require_once "Staff.php";

class ConvertSale extends Model implements Staff
{
    protected $table = "convertsale";
    private $attributes = [
        'id',
        'name',
        'luongcung',
        'kpi'
    ];
    public function getSalary()
    {
        $kpi = $this->kpi;
        if ($kpi > 100) {
            return $this->luongcung + ($kpi - 100) * 15000;
        } elseif ($kpi >= 80) {
            return $this->luongcung - (100 - $kpi) * 10000;
        } else {
            return $this->luongcung - (100 - $kpi) * 15000;
        }
    }
}

?>