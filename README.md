# PhamLuan Mô hình OOP

***Nội dung***

- hiểu được OOP là gì?
- nắm rõ các tính chất của OOP
- phân biệt interface và abstract class
- tham
  khảo [Xuanthulab: lập trình hướng đối tượng với PHP](https://xuanthulab.net/lap-trinh-huong-doi-tuong-oop-voi-php.html)

thực hiện bởi [Phạm Luận](https://www.facebook.com/phluann)

## Kiến thức nắm được

- OOP - lập trình hướng đối tượng, tập trung vào việc tạo ra các đối tượng trong đó chứa
  các thuộc tính và các hàm để thao tác với nó

***4 tính chất của OOP***

- tính đóng gói:
    - các thuộc tính và phương thức có liên quan với nhau được đóng gói thành các lớp để tiện cho việc quản lí và sử
      dụng
    - toàn bộ dữ liệu được chứa trong đối tượng và do đối tượng xử lí, biến đổi mà người dùng
      không cần biết
- tính đa hình:
    - các đối tượng khác nhau có thể cùng thực hiện một công việc theo những cách khác nhau
    - tính đa hình được thể hiện qua:
        - các lớp kế thừa cùng một lớp cha nào đó
        - phương thức đa hình phải được ghi đè ở lớp con
- tính trừu tượng: các lớp được định nghĩa theo cách bao quát (chỉ đề cập đến các phương thức, dữ liệu cần thiết)
- tính kế thừa: cho phép lớp cha có thể chia sẻ dữ liệu và phương thức cho các lớp

***Các thuộc tính***

- private: chỉ sử dụng được bên trong class khai báo nó, không hỗ trợ kế thừa
- protected: các đối tượng bên ngoài không thể gọi đến các thuộc tính hoặc phương thức này,
  chỉ có thể gọi qua việc kế thừa
- public: các đối tượng bên ngoài có thể gọi đến các thuộc tính hoặc phương thức này, có hỗ trợ kế thừa
- static: trong cùng một chương trình, cho dù được gọi ở bất kì file nào thì nó đều giữ lại giá trị
  cuối cùng mà nó được thực hiện vào class. phương thức static có thể được gọi thông qua tên lớp
  mà không cần khởi tạo đối tượng

***extends và implements***

- extends: một lớp kế thừa từ một lớp khác, (chỉ có thể extends từ một class khác)
- implements: kế thừa từ các interface, class con phải override tất cả các phương thức của interface

***interface và abstract class***

- interface:
    - là một khuôn mẫu, không phải một lớp đối tượng, được sử dụng khi một nhóm đối tượng không có cùng
      các thuộc tính nhưng thực hiện các hành động giống nhau
    - các hàm trong interface đều ở dạng khai báo và không được định nghĩa
    - một đối tượng implements từ một interface thì nó phải định nghĩa lại tất cả các hàm trong interface
    - một interface có thể kế thừa từ một hoặc nhiều interface khác
    - không có contructor và destructor
    - phạm vi truy cập mặc định phải là public
- abstract class:
    - là một class, được sử dụng khi một nhóm đối tượng có cùng các thuộc tính, phương thức
    - khi một class kế thừa từ abstract class thì phải định nghĩa lại tất cả các
      phương thức abstract, có thể để phương thức rỗng
    - một abstract class có thể kế thừa từ một lớp và nhiều interface
    - có constructor và destructor
    - có thể xác định phạm vi truy cập cho biến và phương thức

## Thực hành

- xây dựng repo có cấu trúc sau:
    - Model.php: là base kết nối và thực thi SQL
        - Các thuộc tính: $table, $select, $whereClause, $orderClause, $groupClause, $connection
        - Các phương thức: connect(), query(), getFirst(), getAll(), get(), where(), orderBy(), groupBy()
    - Staff.php: Đối tượng nhân viên
        - Các phương thức: getSalary()
    - ConvertSale.php: Đối tượng nhân viên khối ConvertSale
        - Công thức tính lương:
        - KPI: 100 link
        - Lương cứng nếu đủ KPI: 6.000.000đ
        - Thiếu KPI: 1 - 20 phạt 10.000đ/link, 21 - 100 phạt 15.000d/link
        - Thừa KPI: 15.000đ/link
    - IT.php: Đối tượng nhân viên khối IT
        - Công thức tính lương:
        - Lương cứng: 8.000.000đ
        - Thưởng: tùy tháng
    - index.php: Thực thi lấy ra lương của 5 nhân viên trong tháng
