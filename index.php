<?php
    require_once "ConvertSale.php";
    require_once "IT.php";

    echo "IT salary: \n";
    print_r(itSalary());
//    echo "\nconvertSale Salary: \n";
//    print_r(convertSalerSalary());

    function convertSalerSalary()
    {
        $result = [];
        $converSales = ConvertSale::query()->get(5);
        foreach ($converSales as $con){
            $result[] = ["hoten"=>$con->data['name'], "luong"=>$con->getSalary()];
        }
        usort($result, "sortBySalary");
        return $result;
    }

    function itSalary()
    {
        $result = [];
        $its = IT::query()->orderBy("thuong", "desc")->get(5);
        foreach ($its as $it){
            $result[] = ["hoten"=>$it->data["name"], "luong"=>$it->getSalary()];
        }
        usort($result, "sortBySalary");
        return $result;
    }

    function sortBySalary($a,  $b){
        if ($a['luong'] == $b['luong']) return 0;
        return ($a['luong'] > $b['luong']) ? -1 : 1;
    }
?>